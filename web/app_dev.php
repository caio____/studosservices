<?php

ini_set('display_errors', 1);
ini_set('memory_limit', '-1');
set_time_limit(120);
umask(0000);

// This check prevents access to debug front controllers that are deployed by accident to production servers.
// Feel free to remove this, extend it, or make something more sophisticated.
if (isset($_SERVER['HTTP_CLIENT_IP'])
    || isset($_SERVER['HTTP_X_FORWARDED_FOR'])
    || !in_array(@$_SERVER['REMOTE_ADDR'], array('127.0.0.1', 'fe80::1', '::1'))
) {
    header('HTTP/1.0 403 Forbidden');
    exit('You are not allowed to access this file. Check '.basename(__FILE__).' for more information.');
}

require_once __DIR__ . '/../vendor/autoload.php';

\Symfony\Component\Debug\Debug::enable();

$app = new \Studos\Component\Kernel\Application();

require __DIR__ . '/../data/config/config_dev.php';

$app->register(new \Studos\Provider\ApplicationProvider());
$app->run();
