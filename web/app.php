<?php

ini_set('display_errors', 0);
set_time_limit(120);
umask(0000);

require_once __DIR__ . '/../vendor/autoload.php';

$app = new \Studos\Component\Kernel\Application();

require __DIR__ . '/../data/config/config_prod.php';

$app->register(new \Studos\Provider\ApplicationProvider());
$app->run();
