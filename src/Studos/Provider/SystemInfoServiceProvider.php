<?php

namespace Studos\Provider;

use Pimple\Container;
use Silex\Application;
use Pimple\ServiceProviderInterface;
use Studos\Component\SystemInfo\SystemInfo;

class SystemInfoServiceProvider implements ServiceProviderInterface
{
    public function register(Container $container)
    {
        $container['system_info'] = function (Application $app) {
            return new SystemInfo(
                $app['config']['system_info']['system_path'] ?? '/',
                $app['config']['system_info']['cache_path'] ?? sys_get_temp_dir(),
                $app['config']['system_info']['log_path'] ?? sys_get_temp_dir()
            );
        };
    }
}
