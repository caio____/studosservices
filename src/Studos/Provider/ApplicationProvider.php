<?php

namespace Studos\Provider;

use Pimple\Container;
use Studos\Controller;
use Pimple\ServiceProviderInterface;
use JMS\Serializer\SerializerBuilder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Exception\InvalidParameterException;

class ApplicationProvider implements ServiceProviderInterface
{
    public function register(Container $container)
    {
        $this->registerServices($container);
        $this->registerControllers($container);
        $this->bootMiddlewares($container);
    }

    private function registerControllers(Container $container)
    {
        $container->mount('/v1/exam', new Controller\ExamController());
        $container->mount('/v1/status', new Controller\StatusController());
    }

    private function registerServices(Container $container)
    {
        $container->register(new \Silex\Provider\ServiceControllerServiceProvider());
        $container->register(new \Silex\Provider\HttpFragmentServiceProvider());
        $container->register(new \Silex\Provider\FormServiceProvider());
        $container->register(new \Silex\Provider\ValidatorServiceProvider());
        $container->register(new \Silex\Provider\HttpCacheServiceProvider());
        $container->register(new \Silex\Provider\MonologServiceProvider(), $container['config']['monolog']);
        $container->register(new \Knp\Provider\ConsoleServiceProvider(), $container['config']['console']);
        $container->register(new \Silex\Provider\DoctrineServiceProvider(), $container['config']['database']['doctrine']['dbal']);
        $container->register(new \Dflydev\Provider\DoctrineOrm\DoctrineOrmServiceProvider(), $container['config']['database']['doctrine']['orm']);

        $container->register(new SystemInfoServiceProvider());

        if ($container['debug'] == true) {
        }
    }

    private function bootMiddlewares(Container $container)
    {
        $container->view(function($controllerResult, Request $request) use ($container) {

            if ($controllerResult instanceof \Exception) {
                throw $controllerResult;
            }

            $contentType = $request->query->get('contentType', 'json');
            if ($contentType != 'json' && $contentType != 'xml') {
                throw new InvalidParameterException(sprintf(
                    'The content type "%" is invalid! Please use "json" or "xml".',
                    $contentType
                ));
            }

            if (empty($controllerResult)) {
                return New Response(null, 204);
            }

            $serializer = SerializerBuilder::create()->build();

            return new Response($serializer->serialize($controllerResult, $contentType), 200, [
                'Content-Type' => $request->getMimeType($contentType)
            ]);
        });

        $container->after(function (Request $request, Response $response) use ($container) {

            $response->headers->set('Access-Control-Allow-Origin', '*');
            $response->headers->set('Access-Control-Allow-Methods', 'POST, GET, PUT, OPTIONS, PATCH, DELETE');
            $response->headers->set('Access-Control-Allow-Headers', 'access-control-allow-headers,access-control-allow-origin,content-type');

            if ($request->getMethod() == 'OPTIONS') {
                $response->setStatusCode(204);
                $response->setContent(null);
            }

            return $response;
        });

        $container->error(function (\Exception $exception, Request $request, $code) use ($container) {

            if ($container['env'] != 'prod') {
                return $exception;
            }

            return $container->json(
                [
                    'code'    => $exception->getCode(),
                    'message' => $exception->getMessage(),
                ],
                $exception->getCode()
            );
        });
    }
}
