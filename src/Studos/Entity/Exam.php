<?php

namespace Studos\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="exam")
 * @ORM\Entity(repositoryClass="Studos\Repository\Exam")
 */
class Exam extends AbstractEntity
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     */
    protected $name;

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }
}
