<?php

namespace Studos\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="exam_simulation_question_answer")
 * @ORM\Entity(repositoryClass="Studos\Repository\ExamSimulationQuestionAnswer")
 */
class ExamSimulationQuestionAnswer extends AbstractEntity
{
    /**
     * @var string
     *
     * @ORM\Column(name="answer", type="string")
     */
    protected $answer;

    /**
     * @var boolean
     *
     * @ORM\Column(name="correct", type="boolean")
     */
    protected $correct;

    /**
     * @var \Studos\Entity\ExamSimulationQuestion
     *
     * @ORM\ManyToOne(targetEntity="Studos\Entity\ExamSimulationQuestion", inversedBy="examSimulationQuestionAnswers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="examSimulationQuestionId", referencedColumnName="id")
     * })
     */
    protected $examSimulationQuestion;

    public function getAnswer(): string
    {
        return $this->answer;
    }

    public function setAnswer(string $answer): self
    {
        $this->answer = $answer;

        return $this;
    }

    public function isCorrect(): bool
    {
        return $this->correct;
    }

    public function setCorrect(bool $correct): self
    {
        $this->correct = $correct;

        return $this;
    }

    public function getExamSimulationQuestion(): ExamSimulationQuestion
    {
        return $this->examSimulationQuestion;
    }

    public function setExamSimulationQuestion(ExamSimulationQuestion $examSimulationQuestion): self
    {
        $this->examSimulationQuestion = $examSimulationQuestion;

        return $this;
    }
}
