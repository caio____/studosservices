<?php

namespace Studos\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks
 */
class AbstractEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="createdBy", type="integer")
     */
    protected $createdby = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="updatedBy", type="integer")
     */
    protected $updatedby = 0;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="createdAt", type="datetime")
     */
    protected $createdat;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updatedAt", type="datetime")
     */
    protected $updatedat;

    /**
     * @var boolean
     *
     * @ORM\Column(name="enabled", type="boolean")
     */
    protected $enabled = true;

    protected $by;

    public function __construct()
    {
        $this->createdat = new \DateTime();
        $this->updatedat = new \DateTime();
    }

    public function __clone()
    {
        $this->id = null;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreatedBy(): int
    {
        return $this->createdby;
    }

    public function setCreatedBy($createdBy): self
    {
        $this->createdby = $createdBy;

        return $this;
    }

    public function getUpdatedBy(): int
    {
        return $this->updatedby;
    }

    public function setUpdatedBy($updatedBy): self
    {
        $this->updatedby = $updatedBy;

        return $this;
    }

    public function getCreatedAt(): \DateTime
    {
        return $this->createdat;
    }

    public function setCreatedAt(\DateTime $createdAt = null): self
    {
        $this->createdat = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedat;
    }

    public function setUpdatedAt(\DateTime $updatedAt = null): self
    {
        $this->updatedat = $updatedAt;

        return $this;
    }

    public function isEnabled(): bool
    {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self
    {
        $this->enabled = $enabled;

        return $this;
    }

    public function getBy(): int
    {
        return $this->updatedby;
    }

    public function setBy($by): self
    {
        if (is_null($this->id)) {
            $this->createdby = $by;
            $this->createdat = new \DateTime();
        }

        $this->updatedby = $by;
        $this->updatedat = new \DateTime();

        return $this;
    }

    /**
     * @ORM\PreUpdate
     */
    public function preUpdate()
    {
        $this->setUpdatedAt(new \DateTime());
    }
}
