<?php

namespace Studos\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="exam_simulation")
 * @ORM\Entity(repositoryClass="Studos\Repository\ExamSimulation")
 */
class ExamSimulation extends AbstractEntity
{
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     */
    protected $name;

    /**
     * @var string
     *
     * @ORM\Column(name="version", type="string")
     */
    protected $version;

    /**
     * @var \Studos\Entity\Exam
     *
     * @ORM\ManyToOne(targetEntity="Studos\Entity\Exam")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="examId", referencedColumnName="id")
     * })
     */
    protected $exam;

    public function __construct()
    {
        $this->version = '1.0.0';
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getVersion(): string
    {
        return $this->version;
    }

    public function setVersion(string $version): self
    {
        $this->version = $version;

        return $this;
    }

    public function getExam(): Exam
    {
        return $this->exam;
    }

    public function setExam(Exam $exam): self
    {
        $this->exam = $exam;

        return $this;
    }
}
