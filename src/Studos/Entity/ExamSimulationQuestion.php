<?php

namespace Studos\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Table(name="exam_simulation_question")
 * @ORM\Entity(repositoryClass="Studos\Repository\ExamSimulationQuestion")
 */
class ExamSimulationQuestion extends AbstractEntity
{
    const ANSWER_TYPE_ESSAY_QUESTION = 'essay_questions';

    /**
     * @var string
     *
     * @ORM\Column(name="question", type="string")
     */
    protected $question;

    /**
     * @var string
     *
     * @ORM\Column(name="answerType", type="string")
     */
    protected $answerType;

    /**
     * @var \Studos\Entity\ExamSimulation
     *
     * @ORM\ManyToOne(targetEntity="Studos\Entity\ExamSimulation")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="examSimulationId", referencedColumnName="id")
     * })
     */
    protected $examSimulation;

    /**
     * @var \Studos\Entity\ExamSimulationQuestionDiscipline
     *
     * @ORM\ManyToOne(targetEntity="Studos\Entity\ExamSimulationQuestionDiscipline")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="examSimulationQuestionDisciplineId", referencedColumnName="id")
     * })
     */
    protected $examSimulationQuestionDiscipline;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Studos\Entity\ExamSimulationQuestionAnswer", mappedBy="examSimulationQuestion", cascade={"all"})
     * @ORM\JoinColumns({
     *     @ORM\JoinColumn(name="examSimulationQuestionAnswersId", referencedColumnName="id")
     * })
     */
    protected $examSimulationQuestionAnswers;

    public function __construct()
    {
        $this->answerType = self::ANSWER_TYPE_ESSAY_QUESTION;
        $this->examSimulationQuestionAnswers = new ArrayCollection();
    }

    public function getQuestion(): string
    {
        return $this->question;
    }

    public function setQuestion(string $question): self
    {
        $this->question = $question;

        return $this;
    }

    public function getAnswerType(): string
    {
        return $this->answerType;
    }

    public function setAnswerType(string $answerType): self
    {
        $this->answerType = $answerType;

        return $this;
    }

    public function getExamSimulation(): ExamSimulation
    {
        return $this->examSimulation;
    }

    public function setExamSimulation(ExamSimulation $examSimulation): self
    {
        $this->examSimulation = $examSimulation;

        return $this;
    }

    public function getExamSimulationQuestionDiscipline(): ExamSimulationQuestionDiscipline
    {
        return $this->examSimulationQuestionDiscipline;
    }

    public function setExamSimulationQuestionDiscipline(ExamSimulationQuestionDiscipline $examSimulationQuestionDiscipline): self
    {
        $this->examSimulationQuestionDiscipline = $examSimulationQuestionDiscipline;

        return $this;
    }

    public function getExamSimulationQuestionAnswers(): Collection
    {
        return $this->examSimulationQuestionAnswers;
    }

    public function getExamSimulationQuestionAnswer(ExamSimulationQuestionAnswer $examSimulationQuestionAnswers = null): self
    {
        if (!$this->examSimulationQuestionAnswers->contains($examSimulationQuestionAnswers)) {
            $this->examSimulationQuestionAnswers->add($examSimulationQuestionAnswers);
        }

        return $this;
    }

    public function setExamSimulationQuestionAnswer(ExamSimulationQuestionAnswer $examSimulationQuestionAnswers = null): self
    {
        if ($this->examSimulationQuestionAnswers->contains($examSimulationQuestionAnswers)) {
            $this->examSimulationQuestionAnswers->removeElement($examSimulationQuestionAnswers);
        }

        return $this;
    }
}
