<?php

namespace Studos\Controller;

use Silex\Application;
use Silex\Api\ControllerProviderInterface;

class ExamController implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $cf = $app['controllers_factory'];

        $cf->match('/', [$this, 'fetchAllExamsAction'])
            ->method('GET|OPTIONS');

        $cf->match('/{examId}', [$this, 'fetchOneExamAction'])
            ->method('GET|OPTIONS');

        $cf->match('/{examId}/simulation/', [$this, 'fetchAllSimulationsAction'])
            ->method('GET|OPTIONS');

        $cf->match('/{examId}/simulation/{simulationId}', [$this, 'fetchOneSimulationAction'])
            ->method('GET|OPTIONS');

        $cf->match('/{examId}/simulation/{simulationId}/question', [$this, 'fetchAllQuestionsAction'])
            ->method('GET|OPTIONS');

        $cf->match('/{examId}/simulation/{simulationId}/question/{questionId}', [$this, 'fetchOneQuestionAction'])
            ->method('GET|OPTIONS');

        return $cf;
    }

    public function fetchAllExamsAction(Application $app)
    {
        return $app['orm.em']
            ->getRepository('ORMEntity:Exam')
            ->findByEnabled(true);
    }

    public function fetchOneExamAction(Application $app, $examId)
    {
        return $app['orm.em']
            ->getRepository('ORMEntity:Exam')
            ->findOneById($examId);
    }

    public function fetchAllSimulationsAction(Application $app, $examId)
    {
        return $app['orm.em']
            ->getRepository('ORMEntity:ExamSimulation')
            ->findBy([
                'exam'    => $examId,
                'enabled' => true
            ]);
    }

    public function fetchOneSimulationAction(Application $app, $examId, $simulationId)
    {
        return $app['orm.em']
            ->getRepository('ORMEntity:ExamSimulation')
            ->findOneBy([
                'id'   => $simulationId,
                'exam' => $examId
            ]);
    }

    public function fetchAllQuestionsAction(Application $app, $examId, $simulationId)
    {
        return $app['orm.em']
            ->getRepository('ORMEntity:ExamSimulationQuestion')
            ->findByExamAndSimulation($examId, $simulationId);
    }

    public function fetchOneQuestionAction(Application $app, $examId, $simulationId, $questionId)
    {
        return $app['orm.em']
            ->getRepository('ORMEntity:ExamSimulation')
            ->findOneByExamAndSimulationAndQuestion($examId, $simulationId, $questionId);
    }
}
