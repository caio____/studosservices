<?php

namespace Studos\Controller;

use Silex\Application;
use Silex\Api\ControllerProviderInterface;

class StatusController implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $cf = $app['controllers_factory'];

        $cf->match('/', [$this, 'getAction'])
            ->method('GET|OPTIONS');

        return $cf;
    }

    public function getAction(Application $app)
    {
        $stats  = $app['system_info']->stats();
        $status = true;

        return array_merge(
            $stats,
            ['status' => $status]
        );
    }
}
