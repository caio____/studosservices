<?php

namespace Studos\Repository;

use Doctrine\ORM\EntityRepository;

class ExamSimulationQuestion extends EntityRepository
{
    public function findByExamAndSimulation($exam, $examSimulation)
    {
        return $this->createQueryBuilder('esq')
            ->select('esqa, esqd, esq, es, e')
            ->join('esq.examSimulation', 'es')
            ->join('es.exam', 'e')
            ->join('esq.examSimulationQuestionDiscipline', 'esqd')
            ->join('esq.examSimulationQuestionAnswers', 'esqa', 'WITH', 'esqa.enabled = true')
            ->where('esq.enabled = true')
            ->andWhere('e.id = :exam AND es.id = :examSimulation')
            ->setParameters([
                'exam'           => $exam,
                'examSimulation' => $examSimulation
            ])
            ->getQuery()
            ->getResult();
    }

    public function findOneByExamAndSimulationAndQuestion($exam, $examSimulation, $examSimulationQuestion)
    {
        return $this->createQueryBuilder('esq')
            ->select('esqa, esqd, esq, es, e')
            ->join('esq.examSimulation', 'es')
            ->join('es.exam', 'e')
            ->join('esq.examSimulationQuestionDiscipline', 'esqd')
            ->join('esq.examSimulationQuestionAnswers', 'esqa', 'WITH', 'esqa.enabled = true')
            ->where('e.id = :exam AND es.id = :examSimulation AND esq.id = :id')
            ->setParameters([
                'id'             => $examSimulationQuestion,
                'exam'           => $exam,
                'examSimulation' => $examSimulation
            ])
            ->getQuery()
            ->getOneOrNullResult();
    }
}
