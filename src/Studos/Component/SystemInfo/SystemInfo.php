<?php

namespace Studos\Component\SystemInfo;

class SystemInfo
{
    private $systemPath;

    private $cachePath;

    private $logPath;

    public function __construct($systemPath = '/', $cachePath, $logPath)
    {
        $this->systemPath = $systemPath;
        $this->cachePath  = $cachePath;
        $this->logPath    = $logPath;
    }

    public function isWritable()
    {
        return [
            'cache'     => is_writable($this->cachePath),
            'log'       => is_writable($this->logPath),
            'system'    => is_writable(sys_get_temp_dir())
        ];
    }

    public function isReadable()
    {
        return [
            'cache'     => is_readable($this->cachePath),
            'log'       => is_readable($this->logPath),
            'system'    => is_readable(sys_get_temp_dir())
        ];
    }

    public function diskFreeSpace()
    {
        return disk_free_space($this->systemPath);
    }

    public function diskTotalSpace()
    {
        return disk_total_space($this->systemPath);
    }

    public function diskUsedSpace()
    {
        return $this->diskTotalSpace() - $this->diskFreeSpace();
    }

    public function cpuCores()
    {
        return trim(shell_exec("grep -P '^processor' /proc/cpuinfo|wc -l"));
    }

    public function cpuUsage()
    {
        return round(sys_getloadavg()[1] / ($this->cpuCores() + 1) * 100, 0);
    }

    public function memoryStats()
    {
        $data = explode("\n", trim(file_get_contents("/proc/meminfo")));

        $memInfo = [];
        foreach ($data as $line) {
            list($key, $val) = explode(':', $line);
            $memInfo[$key]   = trim(str_replace('kB', '', $val)) * 1024;
        }

        return $memInfo;
    }

    public function stats()
    {
        $diskFreeSpace  = $this->diskFreeSpace();
        $diskTotalSpace = $this->diskTotalSpace();
        $diskUsedSpace  = $this->diskUsedSpace();
        $memoryStats    = $this->memoryStats();

        $freeMem = ($memoryStats['MemFree'] + $memoryStats['Buffers'] + $memoryStats['Cached']);
        $usedMem = ($memoryStats['MemTotal'] - $freeMem);

        return [
            'server' => [
                'cpu' => [
                    'cores'             => $this->cpuCores(),
                    'used_percentage'   => $this->cpuUsage()
                ],
                'memory' => [
                    'total'         => self::humanSize($memoryStats['MemTotal']),
                    'free'          => self::humanSize($freeMem),
                    'used'          => self::humanSize($usedMem),
                    'buffers'       => self::humanSize($memoryStats['Buffers']),
                    'cached'        => self::humanSize($memoryStats['Cached']),
                    'swap_total'    => self::humanSize($memoryStats['SwapTotal']),
                    'swap_free'     => self::humanSize($memoryStats['SwapFree']),
                    'used_percentage' => 100 - (floor(100 * $freeMem / $memoryStats['MemTotal']))
                ],
                'disk' => [
                    'size'            => self::humanSize($diskTotalSpace),
                    'used'            => self::humanSize($diskUsedSpace),
                    'used_percentage' => 100 - (floor(100 * $diskFreeSpace / $diskTotalSpace))
                ]
            ],
            'application' => [
                'is_writable' => $this->isWritable(),
                'is_readable' => $this->isReadable()
            ]
        ];
    }

    public static function humanSize($bytes)
    {
        $prefix = ['B', 'KB', 'MB', 'GB', 'TB'];
        $base   = 1024;
        $class  = min((int)log($bytes, 1024), count($prefix) - 1);

        $denominator = (pow($base, $class));

        if ($denominator == 0) {
            return '0';
        }

        $division = $bytes / $denominator;

        return sprintf('%1.2f', $division) . ' ' . $prefix[$class];
    }
}
