<?php

namespace Studos\Component\Kernel;

use \Silex\Application as BaseApplication;

class Application extends BaseApplication
{
    use BaseApplication\SecurityTrait;
    use BaseApplication\FormTrait;
    use BaseApplication\UrlGeneratorTrait;
    use BaseApplication\MonologTrait;
}
