# Studos Services Project

This project provide a Studos apis services

***

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development or production. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

This project will be made by use 'Ubuntu Server 16.04 LTS' Linux distribution, PHP 7 and MySql 5.7.

***

### Installing

##### Install basic packages
```
$ sudo apt-get update
$ sudo apt-get install git vim htop python-software-properties
```

##### Install Web Server (Nginx)
```
$ sudo apt-get install nginx
```

##### Install Database Server (Mysql)
This step is not required. If you use external database server skip this instruction.
```
$ sudo apt-get install mysql-server
```

##### Install PHP 7 PPA repository
```
$ sudo add-apt-repository ppa:ondrej/php
$ sudo apt-get update
$ sudo apt-get install php7.1 php7.1-fpm php7.1-cli php7.1-common php7.1-curl php7.1-gm php7.1-intl php7.1-json php7.1-mbstring php7.1-mcrypt php7.1-mysql php7.1-sqlite3 php7.1-xml php7.1-zip php7.1-gd
```

If you use dev environment install this packages
```
$ sudo apt-get install php7.1-dev php-xdebug
```

##### Install PHP Composer
```
$ php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
$ sudo php composer-setup.php --install-dir=/bin --filename=composer
$ php -r "unlink('composer-setup.php');"
$ composer config --global process-timeout 99999999 --allow-root
```

##### Clonning the project
Preference clonning the project in public Web Server folder like '/var/www/'.

##### Install project dependencies
```
$ cd /var/www/studos/
$ composer install
```

##### Configure parameters
Copy parameters.php.dist and put your configurations.
```
$ cp data/config/parameters.php.dist data/config/parameters.php
```

##### Write Nginx configuration
Copy and insert this simple configuration in nginx virtual host configuration on '/etc/nginx/sites-enabled/default'.
```
server {
    
    #server_name domain.tld www.domain.tld;
    root /var/www/studos/web;

    location / {
        try_files $uri /app.php$is_args$args;
    }

    # DEV
    location ~ ^/(app_dev)\.php(/|$) {
        fastcgi_pass unix:/var/run/php/php7.1-fpm.sock;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT $realpath_root;
    }

    # PROD
    location ~ ^/app\.php(/|$) {
        fastcgi_pass unix:/var/run/php/php7.1-fpm.sock;
        fastcgi_split_path_info ^(.+\.php)(/.*)$;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $realpath_root$fastcgi_script_name;
        fastcgi_param DOCUMENT_ROOT $realpath_root;
        internal;
    }

    location ~ \.php$ {
        return 404;
    }

    error_log /var/log/nginx/studos_error.log;
    access_log /var/log/nginx/studos_access.log;
}
```

***

## Endpoint References

##### Exam Resources

- **[<code>GET</code> /v1/exam/](data/resources/endpoint-references/GET_V1_EXAMS.md)**
- **[<code>GET</code> /v1/exam/{examId}](data/resources/endpoint-references/GET_V1_EXAM.md)**
- **[<code>GET</code> /v1/exam/{examId}/simulation](data/resources/endpoint-references/GET_V1_EXAM_SIMULATIONS.md)**
- **[<code>GET</code> /v1/exam/{examId}/simulation/{examSimulationId}](data/resources/endpoint-references/GET_V1_EXAM_SIMULATION.md)**
- **[<code>GET</code> /v1/exam/{examId}/simulation/{examSimulationId}/question](data/resources/endpoint-references/GET_V1_EXAM_SIMULATION_QUESTIONS.md)**
- **[<code>GET</code> /v1/exam/{examId}/simulation/{examSimulationId}/question/{examSimulationQuestionId}](data/resources/endpoint-references/GET_V1_EXAM_SIMULATION_QUESTION.md)**

##### Status Resources

- **[<code>GET</code> /v1/status/](data/resources/endpoint-references/GET_V1_STATUS.md)**

***

## Built With

* [Silex](https://silex.symfony.com/) -  PHP Micro Framework
* [NGINX](https://www.nginx.com/) - Web Server
* [MYSQL](https://www.mysql.com/) - Relational Database
* [PHP7](https://php.net/) -  Hypertext Preprocessor Language
* [Composer](https://getcomposer.org/) - PHP Dependency Management
