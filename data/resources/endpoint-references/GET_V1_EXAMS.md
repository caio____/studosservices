# Exams Resources

    GET v1/exam/

## Description
Returns a exams data

***

## Authentication
None

***

## Parameters
Request Query
```
contentType: json|xml
```

***

## Return format
JSON | XML

***

## Example

**Request**
```
curl -X GET http://localhost:8888/app_dev.php/v1/exam/
```

**Return** __shortened for example purpose__
``` json
[
    {
        "id": 1,
        "createdby": 0,
        "updatedby": 0,
        "createdat": "2017-11-23T18:37:17+0000",
        "updatedat": "2017-11-23T18:37:17+0000",
        "enabled": true,
        "name": "Enem"
    }
]
```
