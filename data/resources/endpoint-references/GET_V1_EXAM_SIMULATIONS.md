# Exam Simulations Resources

    GET v1/exam/{examId}/simulation/

## Description
Returns a exam simulations data

***

## Authentication
None

***

## Parameters
Request Query
```
contentType: json|xml
```

***

## Return format
JSON | XML

***

## Example

**Request**
```
curl -X GET http://localhost:8888/app_dev.php/v1/exam/1/simulation/
```

**Return** __shortened for example purpose__
``` json
[
    {
        "id": 1,
        "createdby": 0,
        "updatedby": 0,
        "createdat": "2017-11-23T18:38:54+0000",
        "updatedat": "2017-11-23T18:38:54+0000",
        "enabled": true,
        "name": "2015-2016",
        "version": "1.0.0",
        "exam": {
            "id": 1,
            "createdby": 0,
            "updatedby": 0,
            "createdat": "2017-11-23T18:37:17+0000",
            "updatedat": "2017-11-23T18:37:17+0000",
            "enabled": true,
            "name": "Enem"
        }
    }, {
        "id": 2,
        "createdby": 0,
        "updatedby": 0,
        "createdat": "2017-11-23T18:47:16+0000",
        "updatedat": "2017-11-23T18:47:16+0000",
        "enabled": true,
        "name": "2016-2017",
        "version": "1.0.0",
        "exam": {
            "id": 1,
            "createdby": 0,
            "updatedby": 0,
            "createdat": "2017-11-23T18:37:17+0000",
            "updatedat": "2017-11-23T18:37:17+0000",
            "enabled": true,
            "name": "Enem"
        }
    }, {
        "id": 3,
        "createdby": 0,
        "updatedby": 0,
        "createdat": "2017-11-23T18:47:16+0000",
        "updatedat": "2017-11-23T18:47:16+0000",
        "enabled": true,
        "name": "2017-2018",
        "version": "1.0.0",
        "exam": {
            "id": 1,
            "createdby": 0,
            "updatedby": 0,
            "createdat": "2017-11-23T18:37:17+0000",
            "updatedat": "2017-11-23T18:37:17+0000",
            "enabled": true,
            "name": "Enem"
        }
    }
]
```
