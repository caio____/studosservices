# Status Resources

    GET v1/status/

## Description
Returns a server informations.

***

## Authentication
None

***

## Parameters
Request Query
```
contentType: json|xml
```

***

## Return format
JSON | XML

***

## Example

**Request**
```
curl -X GET http://localhost:8888/app_dev.php/v1/status/
```

**Return** __shortened for example purpose__
``` json
{
    "server": {
        "cpu": {
            "cores": "4",
            "used_percentage": 12
        },
        "memory": {
            "total": "3.89 GB",
            "free": "1021.52 MB",
            "used": "2.89 GB",
            "buffers": "33.23 MB",
            "cached": "184.16 MB",
            "swap_total": "7.81 GB",
            "swap_free": "7.53 GB",
            "used_percentage": 75
        },
        "disk": {
            "size": "118.29 GB",
            "used": "41.92 GB",
            "used_percentage": 36
        }
    },
    "application": {
        "is_writable": {
            "cache": true,
            "log": true,
            "system": true
        },
        "is_readable": {
            "cache": true,
            "log": true,
            "system": true
        }
    },
    "status": true
}
```
