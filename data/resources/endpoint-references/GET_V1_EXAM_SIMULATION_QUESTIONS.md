# Exam Simulation Questions Resources

    GET v1/exam/{examId}/simulation/{examSimulationId}/question/

## Description
Returns a exam simulation questions data

***

## Authentication
None

***

## Parameters
Request Query
```
contentType: json|xml
```

***

## Return format
JSON | XML

***

## Example

**Request**
```
curl -X GET http://localhost:8888/app_dev.php/v1/exam/1/simulation/1/question/
```

**Return** __shortened for example purpose__
``` json
[
    {
        "id": 1,
        "createdby": 0,
        "updatedby": 0,
        "createdat": "2017-11-23T18:55:40+0000",
        "updatedat": "2017-11-23T18:55:40+0000",
        "enabled": true,
        "question": "Quanto \u00e9 2 + 2?",
        "answer_type": "essay_questions",
        "exam_simulation": {
            "id": 1,
            "createdby": 0,
            "updatedby": 0,
            "createdat": "2017-11-23T18:38:54+0000",
            "updatedat": "2017-11-23T18:38:54+0000",
            "enabled": true,
            "name": "2015-2016",
            "version": "1.0.0",
            "exam": {
                "id": 1,
                "createdby": 0,
                "updatedby": 0,
                "createdat": "2017-11-23T18:37:17+0000",
                "updatedat": "2017-11-23T18:37:17+0000",
                "enabled": true,
                "name": "Enem"
            }
        },
        "exam_simulation_question_discipline": {
            "id": 2,
            "createdby": 0,
            "updatedby": 0,
            "createdat": "2017-11-23T18:30:07+0000",
            "updatedat": "2017-11-23T18:30:07+0000",
            "enabled": true,
            "name": "Matem\u00e1tica"
        },
        "exam_simulation_question_answers": [{
            "id": 1,
            "createdby": 0,
            "updatedby": 0,
            "createdat": "2017-11-23T19:02:20+0000",
            "updatedat": "2017-11-23T19:02:20+0000",
            "enabled": true,
            "answer": "3",
            "correct": false
        }, {
            "id": 2,
            "createdby": 0,
            "updatedby": 0,
            "createdat": "2017-11-23T19:02:20+0000",
            "updatedat": "2017-11-23T19:02:20+0000",
            "enabled": true,
            "answer": "4",
            "correct": true
        }, {
            "id": 3,
            "createdby": 0,
            "updatedby": 0,
            "createdat": "2017-11-23T19:02:20+0000",
            "updatedat": "2017-11-23T19:02:20+0000",
            "enabled": true,
            "answer": "5",
            "correct": false
        }, {
            "id": 4,
            "createdby": 0,
            "updatedby": 0,
            "createdat": "2017-11-23T19:02:20+0000",
            "updatedat": "2017-11-23T19:02:20+0000",
            "enabled": true,
            "answer": "2,5",
            "correct": false
        }, {
            "id": 5,
            "createdby": 0,
            "updatedby": 0,
            "createdat": "2017-11-23T19:02:20+0000",
            "updatedat": "2017-11-23T19:02:20+0000",
            "enabled": true,
            "answer": "6",
            "correct": false
        }]
    }
]
```
