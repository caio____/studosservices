<?php declare(strict_types = 1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\DBAL\Migrations\AbstractMigration;

class Version20171123191940 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        $this->addSql(<<<SQL

DROP TABLE IF EXISTS `exam`;
CREATE TABLE `exam` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createdBy` int(11) NOT NULL DEFAULT '0',
  `updatedBy` int(11) NOT NULL DEFAULT '0',
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq1_exam` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `exam_simulation`;
CREATE TABLE `exam_simulation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(180) COLLATE utf8mb4_unicode_ci NOT NULL,
  `version` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '1.0.0',
  `examId` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL DEFAULT '0',
  `updatedBy` int(11) NOT NULL DEFAULT '0',
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq1_exam_simulation` (`name`,`version`,`examId`),
  KEY `idx1_exam_simulation` (`examId`),
  CONSTRAINT `fk1_exam_simulation` FOREIGN KEY (`examId`) REFERENCES `exam` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `exam_simulation_question_discipline`;
CREATE TABLE `exam_simulation_question_discipline` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `createdBy` int(11) NOT NULL DEFAULT '0',
  `updatedBy` int(11) NOT NULL DEFAULT '0',
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniq1_exam_simulation_question_discipline` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `exam_simulation_question`;
CREATE TABLE `exam_simulation_question` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `question` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `answerType` varchar(45) COLLATE utf8mb4_unicode_ci NOT NULL,
  `examSimulationId` int(11) NOT NULL,
  `examSimulationQuestionDisciplineId` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL DEFAULT '0',
  `updatedBy` int(11) NOT NULL DEFAULT '0',
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx1_exam_simulation_question` (`examSimulationId`),
  KEY `idx2_exam_simulation_question` (`examSimulationQuestionDisciplineId`),
  CONSTRAINT `fk1_exam_simulation_question` FOREIGN KEY (`examSimulationId`) REFERENCES `exam_simulation` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk2_exam_simulation_question_discipline` FOREIGN KEY (`examSimulationQuestionDisciplineId`) REFERENCES `exam_simulation_question_discipline` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

DROP TABLE IF EXISTS `exam_simulation_question_answer`;
CREATE TABLE `exam_simulation_question_answer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `answer` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `correct` tinyint(1) NOT NULL,
  `examSimulationQuestionId` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL DEFAULT '0',
  `updatedBy` int(11) NOT NULL DEFAULT '0',
  `createdAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx1_exam_simulation_question_answer` (`examSimulationQuestionId`),
  CONSTRAINT `fk1_exam_simulation_question_answer` FOREIGN KEY (`examSimulationQuestionId`) REFERENCES `exam_simulation_question_discipline` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

INSERT INTO `exam` VALUES (1,'Enem',0,0,'2017-11-23 18:37:17','2017-11-23 18:37:17',1);
INSERT INTO `exam_simulation` VALUES (1,'2015-2016','1.0.0',1,0,0,'2017-11-23 18:38:54','2017-11-23 18:38:54',1),(2,'2016-2017','1.0.0',1,0,0,'2017-11-23 18:47:16','2017-11-23 18:47:16',1),(3,'2017-2018','1.0.0',1,0,0,'2017-11-23 18:47:16','2017-11-23 18:47:16',1);
INSERT INTO `exam_simulation_question_discipline` VALUES (1,'Português',0,0,'2017-11-23 18:30:07','2017-11-23 18:30:07',1),(2,'Matemática',0,0,'2017-11-23 18:30:07','2017-11-23 18:30:07',1),(3,'História',0,0,'2017-11-23 18:30:07','2017-11-23 18:30:07',1),(4,'Geografia',0,0,'2017-11-23 18:30:07','2017-11-23 18:30:07',1),(5,'Biologia',0,0,'2017-11-23 18:30:07','2017-11-23 18:30:07',1),(6,'Física',0,0,'2017-11-23 18:30:07','2017-11-23 18:30:07',1),(7,'Química',0,0,'2017-11-23 18:30:07','2017-11-23 18:30:07',1),(8,'Inglês',0,0,'2017-11-23 18:30:07','2017-11-23 18:30:07',1),(9,'Filosofia',0,0,'2017-11-23 18:30:07','2017-11-23 18:30:07',1),(10,'Sociologia',0,0,'2017-11-23 18:30:07','2017-11-23 18:30:07',1);
INSERT INTO `exam_simulation_question` VALUES (1,'Quanto é 2 + 2?','essay_questions',1,2,0,0,'2017-11-23 18:55:40','2017-11-23 18:55:40',1),(2,'Qual é o sujeito da frase \"Nós vamos ao teatro\"?','essay_questions',1,1,0,0,'2017-11-23 19:04:44','2017-11-23 19:04:44',1),(3,'Qual foi o ano da independência do Brasi?l','essay_questions',1,3,0,0,'2017-11-23 19:07:06','2017-11-23 19:07:06',1),(4,'O que é uma erosão pluvial?','essay_questions',1,4,0,0,'2017-11-23 19:09:04','2017-11-23 19:09:04',1),(5,'O que é uma mitocôndia?','essay_questions',1,5,0,0,'2017-11-23 19:14:03','2017-11-23 19:14:03',1);
INSERT INTO `exam_simulation_question_answer` VALUES (1,'3',0,1,0,0,'2017-11-23 19:02:20','2017-11-23 19:02:20',1),(2,'4',1,1,0,0,'2017-11-23 19:02:20','2017-11-23 19:02:20',1),(3,'5',0,1,0,0,'2017-11-23 19:02:20','2017-11-23 19:02:20',1),(4,'2,5',0,1,0,0,'2017-11-23 19:02:20','2017-11-23 19:02:20',1),(5,'6',0,1,0,0,'2017-11-23 19:02:20','2017-11-23 19:02:20',1),(6,'\"teatro\"',0,2,0,0,'2017-11-23 19:05:41','2017-11-23 19:05:41',1),(7,'\"vamos ao\"',0,2,0,0,'2017-11-23 19:05:41','2017-11-23 19:05:41',1),(8,'\"ao\"',0,2,0,0,'2017-11-23 19:05:41','2017-11-23 19:05:41',1),(9,'\"nós\"',1,2,0,0,'2017-11-23 19:05:41','2017-11-23 19:05:41',1),(10,'\"nós vamos\"',0,2,0,0,'2017-11-23 19:05:41','2017-11-23 19:05:41',1),(11,'1540',0,3,0,0,'2017-11-23 19:08:06','2017-11-23 19:08:06',1),(12,'1678',0,3,0,0,'2017-11-23 19:08:06','2017-11-23 19:08:06',1),(13,'1820',0,3,0,0,'2017-11-23 19:08:06','2017-11-23 19:08:06',1),(14,'1722',0,3,0,0,'2017-11-23 19:08:06','2017-11-23 19:08:06',1),(15,'1822',1,3,0,0,'2017-11-23 19:08:06','2017-11-23 19:08:06',1),(16,'Quando existe a retirada de material da parte superficial do solo pelas águas da chuva.',1,4,0,0,'2017-11-23 19:10:35','2017-11-23 19:10:35',1),(17,'É a erosão causada pelas águas dos rios que provoca desgaste nos planaltos.',0,4,0,0,'2017-11-23 19:11:18','2017-11-23 19:11:18',1),(18,'Quando existe o destacamento e transporte de materiais na forma de partículas do solo ou movimentos de massas do solo de um local para outro sobre a ação da chuva e do escoamento.',0,4,0,0,'2017-11-23 19:12:19','2017-11-23 19:12:19',1),(19,'É um tipo de erosão causada pelo vento com a retirada superficial de fragmentos mais finos.',0,4,0,0,'2017-11-23 19:12:57','2017-11-23 19:12:57',1),(20,'A mitocôndria, é um organismo unicelular com pouca importancia, tem a finalidade de transportar proteínas para as células.',0,5,0,0,'2017-11-23 19:16:36','2017-11-23 19:16:36',1),(21,'A mitocôndria, é uma das organelas celulares mais importantes, sendo extremamente relevante para a respiração celular.\n',1,5,0,0,'2017-11-23 19:16:36','2017-11-23 19:16:36',1),(22,'É  uma organela presente nas células das plantas e outros organismos fotossintetizadores.',0,5,0,0,'2017-11-23 19:17:22','2017-11-23 19:17:22',1);

SQL
        );
    }

    public function down(Schema $schema)
    {
        $this->addSql(<<<SQL

DROP TABLE IF EXISTS `exam_simulation_question_answer`;
DROP TABLE IF EXISTS `exam_simulation_question`;
DROP TABLE IF EXISTS `exam_simulation_question_discipline`;
DROP TABLE IF EXISTS `exam_simulation`;
DROP TABLE IF EXISTS `exam`;

SQL
        );
    }
}
