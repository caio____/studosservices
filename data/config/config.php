<?php

require __DIR__ . '/parameters.php';

$app['config'] = [
    'console' => [
        'console.name' 				=> $app['parameters']['project_name'],
        'console.version' 			=> '1.0.0',
        'console.project_directory' => $app['parameters']['root_dir']
    ],
    'monolog' => [
        'monolog.name' 		=> $app['parameters']['project_name'],
        'monolog.logfile' 	=> $app['parameters']['log_dir'] . '/prod.log',
        'monolog.level' 	=> 'ERROR'
    ],
    'system_info' => [
        'system_path' 	=> '/',
        'cache_path'	=> $app['parameters']['cache_dir'],
        'log_path'		=> $app['parameters']['log_dir']
    ],
    'database' => [
        'doctrine' => [
            'migrations' => [
                'migrations.name' 		=> $app['parameters']['project_name'],
                'migrations.directory' 	=> $app['parameters']['migrations_dir'],
                'migrations.table_name' => 'migration_versions',
                'migrations.namespace' 	=> 'Application\\Migrations'
            ],
            'dbal' => [
                'db.options' => [
                    'driver' 	=> 'pdo_mysql',
                    'dbname' 	=> $app['parameters']['db']['name'],
                    'host' 		=> $app['parameters']['db']['host'],
                    'user' 		=> $app['parameters']['db']['user'],
                    'password' 	=> $app['parameters']['db']['pass'],
                    'charset' 	=> 'utf8mb4',
                    'default_table_options' => [
                        'charset' => 'utf8mb4',
                        'collate' => 'utf8mb4_unicode_ci'
                    ]
                ]
            ],
            'orm' => [
                'orm.proxies_dir' => $app['parameters']['cache_dir'],
                'orm.em.options' => [
                    'mappings' => [
                        [
                            'alias' 	=> 'ORMEntity',
                            'type' 		=> 'annotation',
                            'path' 		=> $app['parameters']['root_dir'] . '/src/Studos/Entity',
                            'namespace' => 'Studos\\Entity',
                            'use_simple_annotation_reader' => false
                        ]
                    ]
                ]
            ]
        ]
    ]
];
