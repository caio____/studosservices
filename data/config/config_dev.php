<?php

require __DIR__ . '/config.php';

$app['env'] = 'dev';

$app['config'] = array_replace_recursive($app['config'], [
	'monolog' => [
  		'monolog.logfile' 	=> $app['parameters']['log_dir'] . '/dev.log',
  		'monolog.level' 	=> 'DEBUG'
	]
]);